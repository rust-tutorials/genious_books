
pub struct Book {
  title: String,
  chapters: Vec<String>
}

trait Summary {
  fn summarize(&self) -> String;
}

impl Summary for Book {
  fn summarize(&self) -> String {
    self.title.to_string() + " (" + &self.chapters.len().to_string() + "ch): " + (&self.chapters.join(" ... "))
  }
}

fn main() {
    let mut book = Book {
      title: "Mut Title".to_string(),
      chapters: vec!["Ch 1".to_string(),
                     "Ch 2".to_string()]
    };
    let sharable_book = Book {
      title: "Share Title".to_string(),
      chapters: vec!["Share Ch 1".to_string(),
                     "Share Ch 2".to_string()]
    };
    assign_title(&mut book, "New Title");
    println!("Hello book: {}", book.title);
    print_title(&book);
    print_title_and_own(sharable_book);
    println!("Chapter count: {}", book.chapters.len());
    println!("Summarize trait: {}", book.summarize());
}


fn assign_title(book: &mut Book, new_title: &str) {
    book.title = new_title.to_string();
}


fn print_title_and_own(book: Book) {
    println!("Transferred Title: {}", book.title);
}

fn print_title(book: &Book) {
    println!("Borrowed Title: {}", book.title);
}

